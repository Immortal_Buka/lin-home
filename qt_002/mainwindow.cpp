#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QBluetoothLocalDevice>
#include <QColorDialog>
#include <QMessageBox>
#include <QThread>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this); 
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    uint8_t buf[8] = {0}, temp_loc, i;
    hid_init();
    hid_handle = hid_open(0xb01a, 0x0001, nullptr);
    if(hid_handle != nullptr)
    {
        hid_read(hid_handle, buf, 8);
        temp_loc = buf[0];
        for(i=0; i<temp_loc; i++)
        {
            buf[1] = i;
            hid_write(hid_handle, buf, 8);
            ui->comboBox->addItem(QString::number(i));
            ui->comboBox->setItemText(i, QString::number(i+1));
            QColor color;
            do
            {
                hid_read(hid_handle, buf, 8);
            } while(buf[1] != i);
            color.setHsv(buf[2]|(buf[3] << 8), buf[4], buf[5]);
            ui->comboBox->setItemData(i, QColor(color), Qt::BackgroundColorRole);
        }
        ui->comboBox->addItem(QString::number(i));
        ui->comboBox->setItemText(temp_loc, "all");
        ui->pushButton_2->setEnabled(true);
        ui->pushButton_3->setEnabled(true);
        ui->horizontalSlider->setEnabled(true);
        ui->pushButton->setEnabled(false);
    }
    else
    {
        QMessageBox messageBox;
        messageBox.critical(this, "Error", "HID open fail!");
        messageBox.setFixedSize(500,200);
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    uint8_t buf[8] = {0};
    int idx = ui->comboBox->currentIndex();
    QColor color = QColorDialog::getColor(Qt::yellow, this);
    if(idx == (ui->comboBox->count()-1))
    {
        for(uint8_t i=0; i<(ui->comboBox->count()-1); i++)
        {
            ui->comboBox->setItemData(i, QColor(color), Qt::BackgroundColorRole);
            buf[0] = 0x16;
            buf[1] = i;
            buf[2] = color.hue() & 0xff;
            buf[3] = color.hue() >> 8;
            buf[4] = color.saturation();
            buf[5] = color.value();
            hid_write(hid_handle, buf, 8);
            hid_read(hid_handle, buf, 8);
        }
    }
    else
    {
        ui->comboBox->setItemData(idx, QColor(color), Qt::BackgroundColorRole);
        buf[0] = 0x16;
        buf[1] = idx;
        buf[2] = color.hue() & 0xff;
        buf[3] = color.hue() >> 8;
        buf[4] = color.saturation();
        buf[5] = color.value();
        hid_write(hid_handle, buf, 8);
        hid_read(hid_handle, buf, 8);
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    uint8_t buf[8] = {0};
    int hue, val;
    QColor color;
    buf[0] = 0x16;
    val = ui->horizontalSlider->value();
    for(uint8_t i=0; i<(ui->comboBox->count()-1); i++)
    {
        hue = 360*i/(ui->comboBox->count()-1);
        color.setHsv(hue, 255, val);
        ui->comboBox->setItemData(i, QColor(color), Qt::BackgroundColorRole);
        buf[1] = i;
        buf[2] = hue & 0xff;
        buf[3] = hue >> 8;
        buf[4] = 0xff;
        buf[5] = val;
        hid_write(hid_handle, buf, 8);
    }
}

void MainWindow::on_actionVersion_triggered()
{
    QMessageBox messageBox;
    messageBox.information(this, "Version", "20190322");
    messageBox.setFixedSize(500,200);
}
