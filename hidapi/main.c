#include <stdio.h>
#include <stdlib.h>
#include "hidapi.h"
//
#define MAX_STR 255
//
int main(int argc, char* argv[])
{
	hid_device *dev = NULL;
	int retval;
	unsigned char buffer[100] = {0};
	wchar_t string[MAX_STR] = {0};
	printf(__DATE__);
	printf("\r\n");
	printf(__TIME__);
	struct hid_device_info *devs, *cur_dev;
	devs = hid_enumerate(0, 0);
	cur_dev = devs;
	while (cur_dev)
	{
		printf("\r\nVID: 0x%04hx", cur_dev->vendor_id);
		printf("\r\nPID: 0x%04hx", cur_dev->product_id);
		printf("\r\npath: %s", cur_dev->path);
		cur_dev = cur_dev->next;
	}
	hid_free_enumeration(devs);
	dev = hid_open(0x1209, 0x2937, NULL);
	if (dev == NULL) printf("\r\nfail to open blue_link");
	else
	{
		printf("\r\nOK to open blue_link");
		if(hid_get_manufacturer_string(dev, string, MAX_STR) == 0) printf("\r\nManufacturer String: %ls", string);
		if(hid_get_product_string(dev, string, MAX_STR) == 0) printf("\r\nProduct String: %ls", string);
		if(hid_get_serial_number_string(dev, string, MAX_STR) == 0) printf("\r\nSerial Number String: %ls", string);
		buffer[0] = 0;
		buffer[1] = 0x99;
		buffer[2] = 0x77;
		//hid_set_nonblocking(dev, 1);
		retval = hid_write(dev, buffer, 4);
		if(retval != 4) printf("\r\nhid_write FAIL, retval: %d", retval);
		else printf("\r\nhid_write OK");
		retval = hid_read_timeout(dev, buffer, 4, 2000);
		if(retval != 4) printf("\r\nhid_read FAIL, retval: %d", retval);
		else
		{
			printf("\r\nhid_read OK");
			for (int i = 0; i < 4; i++) printf("\r\nbuf[%d]: %d", i, buffer[i]);
		}
	}/*
   // Чтение Feature Report из устройства
   buf[0] = 0x2;
   res = hid_get_feature_report(handle, buf, sizeof(buf));
   // Вывод на экран прочитанных данных.
   printf("Feature Report\n   ");
   for (int i = 0; i < res; i++) printf("%02hhx ", buf[i]);
   printf("\n");
   // Установка функции hid_read() в неблокирующий режим.
   hid_set_nonblocking(handle, 1);
   // Послать Output report, чтобы переключить светодиод LED (команда 0x80)
   buf[0] = 1; // В первом байте находится номер репорта
   buf[1] = 0x80;
   res = hid_write(handle, buf, 65);
   // Послать Output report, чтобы запросить состояние (команда 0x81)
   buf[1] = 0x81;
   hid_write(handle, buf, 65);
   // Прочитать запрошенное состояние
   res = hid_read(handle, buf, 65);
   if (res < 0) printf("Unable to read()\n");
   // Вывод на экран прочитанных данных.
   for (int i = 0; i < res; i++) printf("buf[%d]: %d\n", i, buf[i]);*/
   printf("\r\n");
   return 0;
}
