#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSerialPortInfo>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QObject::connect(&serial, &QSerialPort::readyRead, [&]{MainWindow::uart_rx();});
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        ui->comboBox->addItem(info.portName());
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    serial.setBaudRate(115200);
    serial.setPortName(ui->comboBox->currentText());
    serial.open(QIODevice::ReadWrite);
}

void MainWindow::uart_rx()
{
    QByteArray data = serial.readAll();
    ui->label->setText(QString::number(data.length()));
}

void MainWindow::on_pushButton_2_clicked()
{
    serial.close();
}
