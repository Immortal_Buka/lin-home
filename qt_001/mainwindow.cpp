#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QBluetoothLocalDevice>
#include <QColorDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this); 
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QPalette pal = ui->pushButton->palette();
    QColor c1 = QColorDialog::getColor(Qt::yellow, this);
    pal.setColor(QPalette::Button, c1);
    ui->pushButton->setAutoFillBackground(true);
    ui->pushButton->setPalette(pal);
    ui->pushButton->update();
    //c1.setHsv(0,255,255);
    QString converted;
    int rc;
    libusb_device **list;
    libusb_init(&ctx);
    ssize_t count = libusb_get_device_list(ctx, &list);
    ui->text_output_area->clear();
    for(ssize_t idx = 0; idx < count; ++idx)
    {
        libusb_device *device = list[idx];
        libusb_device_descriptor desc;
        rc = libusb_get_device_descriptor(device, &desc);
        assert(rc == 0);
        converted.sprintf("%04x:%04x", desc.idVendor, desc.idProduct);
        ui->text_output_area->append(converted);
    }
    //libusb_device_handle* handle = libusb_open_device_with_vid_pid(ctx, 0x4200, 0x1234);
    //libusb_control_transfer(handle, 0x40, 0x03, 0x1234, 0x000f, NULL, 0, 5000);
}

void MainWindow::on_pushButton_2_clicked()
{
    QBluetoothLocalDevice localDevice;
    QString localDeviceName;
    if(localDevice.isValid())
    {
        localDevice.powerOn();
        localDeviceName = localDevice.name();
        localDevice.setHostMode(QBluetoothLocalDevice::HostDiscoverable);
        QList<QBluetoothAddress> remotes;
        remotes = localDevice.connectedDevices();
        bluetooth_posibility = true;
        //QBluetoothDeviceDiscoveryAgent m_deviceDiscoveryAgent;
        //m_deviceDiscoveryAgent.setLowEnergyDiscoveryTimeout(500);
    }
    else
    {
        bluetooth_posibility = false;
        ui->text_output_area->clear();
        ui->text_output_area->append("Bluetooth not exist");
    }
}
